import axiosClient from "~/apis/axiosClient";
import { SYLLABUS } from "./types";
import Swal from "sweetalert2";
import axios from "axios";

export const getListSyllabus = (filter) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .get2(`/syllabus?page=${filter.page}&size=${filter.size}`)
        .then((response) => {
          if (response) {
            dispatch(setListSyllabus(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getListSyllabusSearch = (filter, keywords) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .get2(
          `/syllabus?keywords=${keywords}&page=${filter.page}&size=${filter.size}`
        )
        .then((response) => {
          if (response) {
            dispatch(getListSyllabusBySearch(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getListSyllabusDate = (filter, start = "", end = "") => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .get2(
          `/syllabus?startDate=${start}&endDate=${end}&page=${filter.page}&size=${filter.size}`
        )
        .then((response) => {
          if (response) {
            dispatch(getListSyllabusByDate(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const duplicateSyllabus = (id, filter) => {
  console.log(filter);
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .postWithId("/syllabus/duplicateSyllabus", `${id}`)
        .then((response) => {
          if (response) {
            dispatch(getListSyllabus(filter));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const deleteSyllabus = (id, filter) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .putWithId("/syllabus/delete-syllabus", `${id}`)
        .then((response) => {
          if (response) {
            dispatch(deleteSyllabusResponse(response));
            dispatch(getListSyllabus(filter));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const sortSyllabus = (options, filter) => {
  console.log(options);
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .get2(
          `/syllabus?page=${filter.page}&size=${filter.size}&sortBy=${options.field}&sortType=${options.type}`
        )
        .then((response) => {
          if (response) {
            dispatch(setListSyllabus(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};
export const getFileDownload = () => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      
      axiosClient.setHeaderAuth(token);
      axios.get(`/syllabus/template/download`,{
        headers: { Accept: "application/vnd.ms-excel" },
        responseType: "blob",})
        .then((response) => {
          if (response) {
            console.log(response)
            const url = window.URL.createObjectURL(response);
            const a = document.createElement("a");
            a.style.display = "none";
            a.href = url;
      // the filename you want
            a.download = "SyllabusTemplate.zip";
            document.body.appendChild(a);
            a.click();
            window.URL.revokeObjectURL(url);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};
export const postSyllabusModal = (file) => {
  return async (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .postFile(`/syllabus/import`, file)
        .then((response) => {
          if (response) {

            console.log("Import file")
            Swal.fire({
              icon: "success",
              title: "Import Syllabus Successfully",
            })
          } 
        })
        .catch((error) => {
          console.log("error",error);
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Import Syllabus Fail!",
          });
        });
    }
  };
};
export const setListSyllabus = (payload) => {
  return {
    type: SYLLABUS.SET_LIST_SYLLABUS,
    payload,
  };
};

export const getListSyllabusBySearch = (payload) => {
  return {
    type: SYLLABUS.GET_LIST_SYLLABUS_BY_SEARCH,
    payload,
  };
};

export const getListSyllabusByDate = (payload) => {
  return {
    type: SYLLABUS.GET_LIST_SYLLABUS_BY_DATE,
    payload,
  };
};

export const deleteSyllabusResponse = (payload) => {
  return {
    type: SYLLABUS.DELETE_SYLLABUS,
    payload,
  };
};

export const getSyllabusItem = (payload) => {
  return {
    type: SYLLABUS.GET_SYLLABUS_ITEM,
    payload,
  };
};

