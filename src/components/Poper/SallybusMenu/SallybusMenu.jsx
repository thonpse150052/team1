import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import styles from "./SyllabusMenu.module.scss";
import HeadlessTippy from "@tippyjs/react/headless";
import Poper from "~/components/Poper";
import { PoperMenuData } from "~/assets/data/SyllabusMenuData";
import ButtonComponent from "~/components/Button";
import { useNavigate } from "react-router-dom";
import config from "~/config";
import { useDispatch } from "react-redux";
import {
  deleteSyllabus,
  duplicateSyllabus,
  getSyllabusItem,
} from "~/redux/actions/syllabusList";
import { Modal } from "antd";
import { useSelector } from "react-redux";

const cx = classNames.bind(styles);

function Menu({ children, hideOnClick = false, id, filter, syllabus }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { syllabusItem } = useSelector((store) => store.syllabus);

  // Modal
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "Do you really want to delete this syllabus?"
  );

  const showModal = () => {
    setOpen(true);
  };

  const handleOnclick = (item) => {
    switch (item.title) {
      case "Add training program": {
        return navigate(config.routes.createProgram);
      }
      case "Duplicate syllabus": {
        return dispatch(duplicateSyllabus(id, filter));
      }
      case "Delete syllabus": {
        showModal();
        break;
      }
      case "Edit syllabus": {
        return dispatch(getSyllabusItem(syllabus));
      }
      default:
        throw new Error("Don't match any title!!");
    }
  };

  const handleOk = () => {
    setModalText("Do you really want to delete this syllabus?");
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);
  };

  if (confirmLoading) {
    dispatch(deleteSyllabus(id, filter));
  }

  const handleCancel = () => {
    setOpen(false);
  };

  const renderResult = (attrs) => (
    <div className={cx("wrapper")} tabIndex="-1" {...attrs}>
      <Poper className={cx("menu-wrapper")}>
        <div className={cx("action-btn")}>
          {PoperMenuData.map((item, index) => (
            <ButtonComponent
              className={cx("button")}
              key={index}
              leftIcon={item.icon}
              onClick={() => handleOnclick(item)}
            >
              {item.title}
            </ButtonComponent>
          ))}
          <Modal
            title="Title"
            open={open}
            onOk={handleOk}
            confirmLoading={confirmLoading}
            onCancel={handleCancel}
          >
            <p>{modalText}</p>
          </Modal>
        </div>
      </Poper>
    </div>
  );

  return (
    <HeadlessTippy
      interactive
      delay={[0, 150]}
      offset={[10, -3]}
      placement="bottom-end"
      hideOnClick={hideOnClick}
      render={renderResult}
    >
      {children}
    </HeadlessTippy>
  );
}

Menu.propTypes = {
  children: PropTypes.node.isRequired,
  hideOnClick: PropTypes.bool,
};

export default Menu;
