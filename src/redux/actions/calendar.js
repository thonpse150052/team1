import axiosClient from "~/apis/axiosClient";
import { CALENDAR } from "./types";

export const getClassLocation = (params) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .getByUrl(`/class_location`)
        .then((response) => {
          if (response) {
            dispatch(setClassLocation(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getClassStatus = (params) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .getByUrl(`/class_status`)
        .then((response) => {
          if (response) {
            dispatch(setClassStatus(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getClassFSU = (params) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .getByUrl(`/fsu`)
        .then((response) => {
          if (response) {
            dispatch(setClassFsu(response));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getClassListByDate = (params) => {
  console.log(params);
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .getByUrl(`/training-class/calendar/get-date?current_date=${params}`)
        .then((response) => {
          if (response) {
            dispatch(setClassList(response.data));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const getClassListAll = (params) => {
  return (dispatch) => {
    const token = axiosClient.getToken();
    if (token) {
      axiosClient.setHeaderAuth(token);
      axiosClient
        .getByUrl(`/training-class/calendar/`)
        .then((response) => {
          if (response) {
            dispatch(setClassList(response.data));
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
};

export const setClassList = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_LIST,
    payload,
  };
};
export const setClassLocation = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_LOCATION,
    payload,
  };
};

export const setClassStatus = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_STATUS,
    payload,
  };
};

export const setClassAttendee = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_ATTENDEE,
    payload,
  };
};

export const setClassFsu = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_FSU,
    payload,
  };
};

export const setFormatType = (payload) => {
  return {
    type: CALENDAR.SET_CLASS_FORMAT_TYPE,
    payload,
  };
};
