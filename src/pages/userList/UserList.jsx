import React, { useState } from "react";
import PersonIcon from "@mui/icons-material/Person";
import { blue } from "@mui/material/colors";
import {
  Box,
  Chip,
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import SortIcon from "@mui/icons-material/Sort";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

import axios from "axios";
import { useEffect } from "react";

import "./UserList.scss";

//sorting func sec.
function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

//source: https://codesandbox.io/s/xmp5ee?file=/demo.js:2206-2653
//source above, end

//

//presentation
const colorStatus = new Map();
colorStatus.set("ON_BOARDING", { name: "On boarding", color: "#D45B13" });
colorStatus.set("OFF_CLASS", { name: "Off class", color: "#8B8B8B" });
colorStatus.set("IN_CLASS", { name: "In class", color: "#2D3748" });
colorStatus.set("ACTIVE", { name: "Active", color: "#2D3748" });
colorStatus.set("DEACTIVE", { name: "Deactive", color: "" });

const colorRole = new Map();
colorRole.set("Student", { name: "Student", color: "#2D3748" });
colorRole.set("Officer", { name: "Officer", color: "#2D3748" });
colorRole.set("Trainer", { name: "Trainer", color: "#2D3748" });
colorRole.set("Class Admin", { name: "Class Admin", color: "#4DB848" });
colorRole.set("Super Admin", { name: "Super Admin", color: "#4DB848" });

function CustomChip(key, values) {
  return (
    <>
      <Chip
        label={values.get(key).name}
        sx={{
          color: "white",
          backgroundColor: values.get(key).color,
          fontSize: 14,
          fontFamily: "Roboto",
          fontWeight: "400",
          paddingLeft: "4px",
          paddingRight: "4px",
        }}
      ></Chip>
    </>
  );
}

function getBirthdate(birthdate) {
  const date = birthdate !== null ? birthdate + "" : "no data";
  return date.slice(0, 10);
}

function UserList() {
  const [page, setPage] = useState(1);
  const [rows, setrows] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
  };

  const token =
    "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20iLCJpYXQiOjE2Njk4NTUwODMsImV4cCI6MTY2OTg2MTA4M30.j2rug1tzXxaOzI9k2DFSsz2pnqffuHNBjWe-scU5tzAsHilrqZtB4QhrYkUfXVMzZvhr8lEmMf3PKDnzQP576g";

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get(
        "http://ec2-13-212-160-147.ap-southeast-1.compute.amazonaws.com/api/v1/users?page=1&size=23",
        config
      )
      .then((data) => setrows(data.data.data));
  }, []);
  console.log(rows);
  return (
    <>
      <div className="userList">
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />

        <TableContainer>
          <Table sx={{ marginTop: "40px" }}>
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Fullname</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Date of Birth</TableCell>
                <TableCell>Gender</TableCell>
                <TableCell>Level</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Options</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(
                  (page - 1) * rowsPerPage,
                  (page - 1) * rowsPerPage + rowsPerPage
                )
                .map((row) => (
                  <TableRow key={row.id}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell
                      sx={{
                        fontFamily: "inter",
                        fontWeight: "600",
                        fontSize: 14,
                      }}
                    >
                      {row.fullname}
                    </TableCell>
                    <TableCell>{row.email}</TableCell>
                    <TableCell>{getBirthdate(row.birthday)}</TableCell>
                    <TableCell>
                      {row.gender === "FEMALE" ? (
                        <PersonIcon
                          sx={{
                            color: "#e74a3b",
                            fontSize: 30,
                          }}
                        />
                      ) : (
                        <PersonIcon
                          sx={{
                            color: blue[900],
                            fontSize: 30,
                          }}
                        />
                      )}
                    </TableCell>
                    <TableCell>{row.level}</TableCell>
                    <TableCell>
                      {CustomChip(row.role.name, colorRole)}
                    </TableCell>
                    <TableCell>{CustomChip(row.status, colorStatus)}</TableCell>
                    <TableCell>
                      <MoreHorizIcon fontSize="medium" />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              paddingTop: "30px",
            }}
          >
            <div></div>
            <Pagination
              count={Math.ceil(rows.length / rowsPerPage)}
              page={page}
              onChange={handleChangePage}
              showFirstButton
              showLastButton
              sx={{
                fontFamily: "Inter",
                fontSize: "14px",
                fontWeight: "600",
              }}
            />

            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page - 1}
              onRowsPerPageChange={handleChangeRowsPerPage}
              onPageChange={handleChangePage}
            />
          </Box>
        </TableContainer>
      </div>
    </>
  );
}

export default UserList;
