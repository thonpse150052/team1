import React, { useEffect, useState } from "react";
import { Checkbox, DatePicker, Select } from "antd";
import Button from "~/components/Button";
import {
  ArrowDownCenterIcon,
  CalendarToDayIcon,
  ClearIcon,
} from "~/components/Icons";
import "./PopupFilter.scss";
import { useDispatch } from "react-redux";
import { getClassLocation, getClassStatus } from "~/redux/actions/calendar";
import { useSelector } from "react-redux";

const { Option } = Select;

const classTimeOption = ["Morning", "Noon", "Night"];
const attendeeOption = [
  "Intern",
  "Fresher",
  "Online fee-fresher",
  "Offline fee-fresher",
];

function PopupFilter({ modalOpen }) {
  const dispatch = useDispatch();

  const classLocation = useSelector((state) => state.calendar.classLocation);
  const classStatus = useSelector((state) => state.calendar.classStatus);
  const classAttendee = useSelector((state) => state.calendar.classAttendee);
  const classFsu = useSelector((state) => state.calendar.classFsu);
  const formatType = useSelector((state) => state.calendar.formatType);

  const [checkClassTime, setCheckClassTime] = useState([]);
  const [checkStatus, setCheckStatus] = useState([]);
  const [checkAttendee, setCheckAttendee] = useState([]);
  const [selectFSU, setSelectFSU] = useState("");
  const [selectTrainer, setSelectTrainer] = useState("");
  const [selectLocation, setSelectLocation] = useState([]);
  const [dateFrom, setDateFrom] = useState("");
  const [dateTo, setDateTo] = useState("");

  const onChangeCheckboxClassTime = (checkedValues) => {
    setCheckClassTime([...checkedValues]);
  };

  const onChangeCheckboxStatus = (checkedValues) => {
    setCheckStatus([...checkedValues]);
  };

  const onChangeCheckboxAttendee = (checkedValues) => {
    setCheckAttendee([...checkedValues]);
  };

  const onChangeDate = (date, dateString) => {
    console.log(dateString);
    console.log(date);
    // setDateFrom(dateString);
  };

  const handleClear = (e) => {
    setCheckAttendee(e.target.checked ? checkAttendee : []);
    setCheckClassTime(e.target.checked ? checkClassTime : []);
    setCheckStatus(e.target.checked ? checkStatus : []);
    setSelectFSU("");
    setSelectTrainer("");
    setSelectLocation([]);
    setDateFrom("");
    setDateTo("");
  };

  const handleSearch = () => {
    const newObj = {
      keyword: ["string"],
      location: selectLocation,
      status: checkStatus,
      attendee: checkAttendee,
      from: "2022-12-07T01:48:06.096Z",
      to: "2022-12-07T01:48:06.096Z",
      classTime: checkClassTime,
      fsu: selectFSU,
      trainer: selectTrainer,
    };

    console.log(newObj);
  };

  return (
    <>
      {modalOpen && (
        <div className="popupfilter">
          <div className="location-time">
            <div className="location">
              <p className="title">Class location</p>
              <Select
                mode="multiple"
                allowClear
                value={selectLocation}
                onChange={(value) => setSelectLocation(value)}
                suffixIcon={<ArrowDownCenterIcon />}
                clearIcon={<ClearIcon />}
                removeIcon=""
                showArrow="true"
              >
                {classLocation.map((location) => (
                  <Option key={location.id} value={location.name}>
                    {location.name}
                  </Option>
                ))}
              </Select>
            </div>
            <div className="time">
              <p className="title">Class time frame</p>
              <div className="from-to">
                <div className="from">
                  <p>from</p>
                  <DatePicker
                    value={dateFrom}
                    placeholder="----/--/--"
                    suffixIcon={<CalendarToDayIcon />}
                    clearIcon={<ClearIcon />}
                    onChange={onChangeDate}
                  />
                </div>
                <div className="to">
                  <p>to</p>
                  <DatePicker
                    value={dateTo}
                    placeholder="----/--/--"
                    suffixIcon={<CalendarToDayIcon />}
                    clearIcon={<ClearIcon />}
                    onChange={onChangeDate}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="check-box">
            <div className="classtime">
              <p className="title">Class time</p>
              <Checkbox.Group
                value={checkClassTime}
                onChange={onChangeCheckboxClassTime}
              >
                {classTimeOption.map((classtime, index) => (
                  <Checkbox key={index} value={classtime}>
                    {classtime}
                  </Checkbox>
                ))}
              </Checkbox.Group>
            </div>
            <div className="status">
              <p className="title">Status</p>
              <Checkbox.Group
                value={checkStatus}
                onChange={onChangeCheckboxStatus}
              >
                {classStatus.map((status) => (
                  <Checkbox key={status.id} value={status.name}>
                    {status.name}
                  </Checkbox>
                ))}
              </Checkbox.Group>
            </div>
            <div className="attend">
              <p className="title">Attendee</p>
              <Checkbox.Group
                value={checkAttendee}
                onChange={onChangeCheckboxAttendee}
              >
                {attendeeOption.map((attendee, index) => (
                  <Checkbox key={index} value={attendee}>
                    {attendee}
                  </Checkbox>
                ))}
              </Checkbox.Group>
            </div>
          </div>

          <div className="select-list">
            <div className="FSU">
              <p className="title">FSU</p>
              <Select
                fieldNames="fsu"
                value={selectFSU}
                className="select"
                suffixIcon={<ArrowDownCenterIcon />}
                onChange={(value) => setSelectFSU(value)}
              >
                {classFsu?.map((fsu) => (
                  <Option key={fsu.id} value={fsu.name}>
                    {fsu.name}
                  </Option>
                ))}
              </Select>
            </div>
            <div className="trainer">
              <p className="title">Trainer</p>
              <Select
                fieldNames="trainer"
                value={selectTrainer}
                className="select"
                suffixIcon={<ArrowDownCenterIcon />}
                onChange={(value) => setSelectTrainer(value)}
              >
                <Option value="Jack1">Jack1</Option>
                <Option value="Lucy1">Lucy1</Option>
                <Option value="yiminghe1">yiminghe1</Option>
              </Select>
            </div>
          </div>

          <div className="button">
            <Button clear onClick={handleClear}>
              Clear
            </Button>
            <Button className="search-btn" primary onClick={handleSearch}>
              Search
            </Button>
          </div>
        </div>
      )}
    </>
  );
}

export default PopupFilter;
