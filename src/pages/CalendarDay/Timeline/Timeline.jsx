import React from "react";
import PropTypes from "prop-types";
import { classData } from "~/assets/data/ClassData";
import Class from "../Class/Class";
import "./Timeline.scss";

function Timeline({ timlineItem }) {
  return (
    <div className="timeline">
      {timlineItem?.times?.map((time, index) => (
        <div key={index} className="line-inner">
          <div className="text-frame">
            <p>{time.show}</p>
          </div>
          <div className="class-list">
            {classData
              .filter((i) => i.time === time.show)
              .map((classItem, index) => (
                <Class key={index} classItem={classItem} />
              ))}
          </div>
        </div>
      ))}
    </div>
  );
}

Timeline.propTypes = {
  timlineItem: PropTypes.object,
};

export default Timeline;
