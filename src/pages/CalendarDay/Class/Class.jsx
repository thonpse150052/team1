import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import HeadlessTippy from "@tippyjs/react/headless";
import classNames from "classnames/bind";
import { GradeIcon, HomeWorkIcon, LectureSmallIcon } from "~/components/Icons";
import styles from "./Class.module.scss";

const cx = classNames.bind(styles);

function Class({ classItem }) {
  const [className, setClassName] = useState("");
  const [classNamePopup, setClassNamePopup] = useState("");
  const [width, setWidth] = useState(0);

  const classRef = useRef();

  useLayoutEffect(() => {
    setWidth(classRef.current.offsetWidth);
  }, []);

  useEffect(() => {
    if (classItem.studyForm === "intern") {
      setClassName("intern");
      setClassNamePopup("intern-popup");
    } else if (classItem.studyForm === "fresher") {
      setClassName("fresher");
      setClassNamePopup("fresher-popup");
    } else if (classItem.studyForm === "online") {
      setClassName("online");
      setClassNamePopup("online-popup");
    } else if (classItem.studyForm === "offline") {
      setClassName("offline");
      setClassNamePopup("offline-popup");
    }
  }, [classItem.studyForm]);

  const renderResult = (attrs) => (
    <div className={cx("class-popup")} tabIndex="-1" {...attrs}>
      <div className={cx("content", classNamePopup)} style={{ width: width }}>
        <div className={cx("name")}>
          <p>Business Analyst Foundation</p>
        </div>
        <div className={cx("lesson")}>
          <div className={cx("day")}>
            <p>Day 10 of 20</p>
          </div>

          <div className={cx("unit")}>
            <div className={cx("inner-unit")}>
              <p>Unit 6</p>
            </div>
            <p className={cx("unit-name")}>MVC Architecture in ASP.NET </p>
          </div>
        </div>
        <div className={cx("infor")}>
          <div className={cx("location")}>
            <div className={cx("tag")}>
              <HomeWorkIcon />
              <p>Location</p>
            </div>
            <p>HN.Fville</p>
          </div>
          <div className={cx("trainee")}>
            <div className={cx("tag")}>
              <LectureSmallIcon />
              <p>Trainer</p>
            </div>
            <p className={cx("link")}>Dinh Vu Quoc Trung</p>
          </div>
          <div className={cx("admin")}>
            <div className={cx("tag")}>
              <GradeIcon />
              <p>Admin</p>
            </div>
            <p className={cx("link")}>Ly Lien Lien Dung</p>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div ref={classRef}>
      <HeadlessTippy
        interactive
        delay={[0, 200]}
        offset={[0, 0]}
        placement="bottom-end"
        render={renderResult}
      >
        <div className={cx("class", className)}>
          <p className={cx("class-code")}>{classItem.code}</p>
          <div className={cx("line")}>|</div>
          <p className={cx("subject")}>{classItem.name}</p>
        </div>
      </HeadlessTippy>
    </div>
  );
}

Class.propTypes = {
  classItem: PropTypes.object,
};

export default Class;
