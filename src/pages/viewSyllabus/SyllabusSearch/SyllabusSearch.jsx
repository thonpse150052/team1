import React, { useState, useRef, useEffect, useCallback } from "react";
import classNames from "classnames/bind";
import {
  SearchOutlined,
  CalendarTwoTone,
  PlusCircleOutlined,
} from "@ant-design/icons";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./datepicker.css";

import styles from "./SyllabusSearch.module.scss";
import ImportModal from "../ImportModal";
import { SearchValueItem } from "~/container/SearchValueItem";
import { useDispatch } from "react-redux";
import {
  getListSyllabus,
  getListSyllabusDate,
  getListSyllabusSearch,
} from "~/redux/actions/syllabusList";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import config from "~/config";
import { Publish } from "@mui/icons-material";

const cx = classNames.bind(styles);

function SyllabusSearch({ filter }) {
  const navigate = useNavigate();

  // Calendar
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  // Search
  const [searchValue, setSearchValue] = useState([]);
  const inputRef = useRef();
  const dispatch = useDispatch();

  const onChangeDateHandler = (value) => {
    setStartDate(value[0]);
    setEndDate(value[1]);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleChange = useCallback(
    () => setIsModalOpen(!isModalOpen),
    [isModalOpen]
  );

  const saveAfterEnter = async (e) => {
    if (e.code === "Enter") {
      await setSearchValue((prev) => [...prev, e.target.value]);
      inputRef.current.value = "";
    }
  };

  const removeSearchTag = (item) => {
    const indexOfItem = searchValue.indexOf(item);
    setSearchValue(searchValue.filter((_, index) => index !== indexOfItem));
  };

  useEffect(() => {
    if (searchValue) {
      dispatch(getListSyllabusSearch(filter, searchValue));
    } else {
      dispatch(getListSyllabus(filter));
    }
  }, [searchValue]);

  useEffect(() => {
    let start = moment(startDate).format("YYYY/MM/DD");
    let end = moment(endDate).format("YYYY/MM/DD");
    if (startDate && endDate) {
      dispatch(getListSyllabusDate(filter, start, end));
    } else {
      dispatch(getListSyllabus(filter));
    }
  }, [startDate, endDate]);

  return (
    <div className={cx("Wrapper")}>
      <div className={cx("search")}>
        <div>
          {searchValue.length <= 1 ? (
            <input
              className={cx("search-input")}
              placeholder="Search by..."
              onKeyDown={saveAfterEnter}
              ref={inputRef}
            />
          ) : (
            <input
              className={cx("search-input")}
              placeholder="Search by..."
              onKeyDown={saveAfterEnter}
              ref={inputRef}
              disabled
            />
          )}
          <SearchOutlined className={cx("search-input-icon")} />
          <div className={cx("search-value-wrapper")}>
            {searchValue.map((item, index) => {
              return (
                <SearchValueItem
                  key={index}
                  item={item}
                  removeSearchTag={removeSearchTag}
                />
              );
            })}
          </div>
        </div>

        <div className={cx("calendar")}>
          <DatePicker
            className={cx("datepicker")}
            selectsRange={true}
            startDate={startDate}
            endDate={endDate}
            onChange={onChangeDateHandler}
            dateFormat="dd/MM/yyyy"
            openToDate={new Date()}
            placeholderText="Created date"
            shouldCloseOnSelect={false}
          />
          <CalendarTwoTone
            twoToneColor="#285D9A"
            className={cx("calendar-icon")}
          />
        </div>
      </div>
      <div className={cx("actions-btn")}>
        <ImportModal
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
        />
        <button
          className={cx("add-btn")}
          onClick={() => navigate(config.routes.createSyllabus)}
        >
          <PlusCircleOutlined className={cx("add-icon")} />
          <span>Add Syllabus</span>
        </button>
      </div>
    </div>
  );
}

export default SyllabusSearch;
