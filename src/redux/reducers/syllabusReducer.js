import { SYLLABUS } from "../actions/types";

const initialState = {
  syllabusList: [],
  status: "",
  message: "",
  syllabusItem: {},
  draft: [],
  level: [],
  delivery: [],
  outputStandard: [],
};

function syllabusReducer(state = initialState, action) {
  switch (action.type) {
    case SYLLABUS.SET_LIST_SYLLABUS:
      return {
        ...state,
        syllabusList: action.payload,
      };
    case SYLLABUS.GET_LIST_SYLLABUS_BY_SEARCH:
      return {
        ...state,
        syllabusList: action.payload,
      };
    case SYLLABUS.GET_LIST_SYLLABUS_BY_DATE:
      return {
        ...state,
        syllabusList: action.payload,
      };
    case SYLLABUS.SORT_SYLLABUS:
      return {
        ...state,
        syllabusList: action.payload,
      };
    case SYLLABUS.DELETE_SYLLABUS:
      return {
        ...state,
        status: action.payload.status,
        message: action.payload.message,
      };
    case SYLLABUS.GET_SYLLABUS_ITEM:
      return {
        ...state,
        syllabusItem: action.payload,
      };
    case SYLLABUS.SET_DRAFT:
      return {
        ...state,
        draft: action.payload,
      };
    case SYLLABUS.SET_LEVEL:
      return {
        ...state,
        level: action.payload,
      };
    case SYLLABUS.SET_DELIVERY:
      return {
        ...state,
        delivery: action.payload,
      };
    case SYLLABUS.SET_OUTPUT_STANDARD:
      return {
        ...state,
        outputStandard: action.payload,
      };
    default:
      return state;
  }
}

export default syllabusReducer;
